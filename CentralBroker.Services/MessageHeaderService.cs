﻿using CentralBroker.Services.Interfaces;
using Hl7.Fhir.Model;
using Hl7.Fhir.Rest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CentralBroker.Services
{
    public class MessageHeaderService : IMessageHeaderService
    {
        FhirClientSettings settings = new FhirClientSettings
        {
            PreferredFormat = ResourceFormat.Json,
            PreferredReturn = Prefer.ReturnRepresentation
        };
        public List<MessageHeader> GetMessageHeaders()
        {
            FhirClient fhirClient = new FhirClient("https://server.fire.ly/", settings);
            List<MessageHeader> messageHeaderList = new List<MessageHeader>();

            Bundle messageHeaderBundle = fhirClient.Search<MessageHeader>();

            foreach (Bundle.EntryComponent entry in messageHeaderBundle.Entry)
            {
                if (entry.Resource is not null)
                {
                    MessageHeader messageHeader = (MessageHeader)entry.Resource;

                    messageHeaderList.Add(messageHeader);
                }
            }
            return messageHeaderList;
        }
        public MessageHeader? GetAMessageHeader(string messageHeaderId)
        {
            FhirClient fhirClient = new FhirClient("https://server.fire.ly/", settings);

            MessageHeader messageHeader = fhirClient.Read<MessageHeader>($"MessageHeader/{messageHeaderId}");

            return messageHeader;
        }
        public void DeleteAMessageHeader(string messageHeaderId)
        {
            FhirClient fhirClient = new FhirClient("https://server.fire.ly/", settings);
            MessageHeader? messageHeaderToDelete = GetAMessageHeader(messageHeaderId);

            if (messageHeaderToDelete is not null)
            {
                fhirClient.Delete($"MessageHeader/{messageHeaderId}");
            }
        }
        public MessageHeader CreateAMessageHeader(string codingSystem, string codingCode, string codingDisplay, string destinationName, string destinationEndpoint, string resourceDestinationRef, string senderReference, string sourceName, string sourceEndpoint, string focusReference)
        {
            FhirClient fhirClient = new FhirClient("https://server.fire.ly/", settings);
            MessageHeader messageHeaderToCreate = new MessageHeader();
            List<Coding> codingList = new List<Coding>();

            Coding coding = new Coding()
            {
                System = "https://fhir.nhs.uk/CodeSystem/NHSEngland-MessageEventType",
                Code = "new",
                Display = "New Event Message"
            };
            codingList.Add(coding);

            Extension extension = new Extension()
            {
                Url = "https://fhir.nhs.uk/StructureDefinition/Extension-NHSEngland-MessageEventType",
                Value = new CodeableConcept() { Coding = codingList }
            };

            messageHeaderToCreate.Id = Guid.NewGuid().ToString();
            messageHeaderToCreate.Meta = new Meta() { ProfileElement = new List<FhirUri> { new FhirUri("https://fhir.hl7.org.uk/StructureDefinition/UKCore-MessageHeader") } };
            messageHeaderToCreate.Event = new Coding() { System = codingSystem, Code = codingCode, Display = codingDisplay };
            messageHeaderToCreate.Destination = new List<MessageHeader.MessageDestinationComponent>() { new MessageHeader.MessageDestinationComponent() {
                Name = destinationName,
                Endpoint = destinationEndpoint,
                Receiver = new ResourceReference() { Reference = resourceDestinationRef }, }
            };
            messageHeaderToCreate.Sender = new ResourceReference() { Reference = senderReference };
            messageHeaderToCreate.Source = new MessageHeader.MessageSourceComponent() { Name = sourceName, Endpoint = sourceEndpoint };
            messageHeaderToCreate.Focus = new List<ResourceReference>() { new ResourceReference() { Reference = focusReference } };

            MessageHeader messageHeader = fhirClient.Create<MessageHeader>(messageHeaderToCreate);

            return messageHeader;
        }
        public MessageHeader UpdateAMessageHeader(string bundleId, string messageHeaderId, string messageHeaderProfile, string codingSystem, string codingCode, string codingDisplay, string destinationName, string destinationEndpoint, string resourceDestinationRef, string senderReference, string sourceName, string sourceEndpoint, string focusReference)
        {
            FhirClient fhirClient = new FhirClient("https://server.fire.ly/", settings);
            //Bundle queriedBundle = new BundleController(_rabbitMQProducer).GetABundle(bundleId);
            MessageHeader? messageHeaderToUpdate = GetAMessageHeader(messageHeaderId);

            messageHeaderToUpdate.Meta = new Meta() { ProfileElement = new List<FhirUri> { new FhirUri(messageHeaderProfile) } };
            messageHeaderToUpdate.Event = new Coding() { System = codingSystem, Code = codingCode, Display = codingDisplay };
            messageHeaderToUpdate.Destination = new List<MessageHeader.MessageDestinationComponent>()
            {
                new MessageHeader.MessageDestinationComponent()
                {
                    Name = destinationName,
                    Endpoint = destinationEndpoint,
                    Receiver = new ResourceReference() { Reference = resourceDestinationRef },
                }
            };
            messageHeaderToUpdate.Sender = new ResourceReference() { Reference = senderReference };
            messageHeaderToUpdate.Source = new MessageHeader.MessageSourceComponent() { Name = sourceName, Endpoint = sourceEndpoint };
            messageHeaderToUpdate.Focus = new List<ResourceReference>() { new ResourceReference() { Reference = focusReference } };

            MessageHeader updatedMessageHeader = fhirClient.Update<MessageHeader>(messageHeaderToUpdate);

            /*var index = queriedBundle.Entry.FindIndex(e => e.Resource.TypeName == "MessageHeader");

            if (index != -1)
            {
                var messageHeaderEntry = new Bundle.EntryComponent()
                {
                    Resource = updatedMessageHeader,
                    FullUrl = updatedMessageHeader.ResourceBase.ToString() + updatedMessageHeader.Id,
                };

                queriedBundle.Entry[index] = messageHeaderEntry;
                Bundle updatedBundle = fhirClient.Update<Bundle>(queriedBundle);
            }*/

            return updatedMessageHeader;
        }
    }
}
