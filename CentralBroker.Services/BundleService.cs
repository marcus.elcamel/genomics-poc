﻿using CentralBroker.Services.Interfaces;
using Hl7.Fhir.Model;
using Hl7.Fhir.Rest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CentralBroker.Services
{
    public class BundleService : IBundleService
    {
        public BundleService() { }
        FhirClientSettings settings = new FhirClientSettings
        {
            PreferredFormat = ResourceFormat.Json,
            PreferredReturn = Prefer.ReturnRepresentation
        };
        public Bundle GetBundles()
        {
            FhirClient fhirClient = new FhirClient("https://server.fire.ly/", settings);

            Bundle bundle = fhirClient.Search<Bundle>();

            return bundle;
        }
        public Bundle GetABundle(string bundleId)
        {
            FhirClient fhirClient = new FhirClient("https://server.fire.ly/", settings);

            Bundle bundle = fhirClient.Read<Bundle>($"Bundle/{bundleId}");
            return bundle;
        }
        public void DeleteABundle(string bundleId)
        {
            FhirClient fhirClient = new FhirClient("https://server.fire.ly/", settings);
            Bundle bundleToDelete = GetABundle(bundleId);

            if (bundleToDelete is not null)
            {
                fhirClient.Delete($"Bundle/{bundleId}");
            }
        }
        public Bundle CreateABundle(
            string bundleIdentifierValue, 
            string serviceReqId, 
            string patientId, 
            string practitionerId, 
            string messageHeaderId, 
            string organisationId, 
            string organisation2Id, 
            string destination)
        {
            FhirClient fhirClient = new FhirClient("https://server.fire.ly/", settings);
            Bundle bundleToCreate = new Bundle();

            var serviceRequest = new ServiceRequestService().GetAServiceRequest(serviceReqId);
            var patient = new PatientService().GetAPatient(patientId);
            var practitioner = new PractitionerService().GetAPractitioner(practitionerId);
            var messageHeader = new MessageHeaderService().GetAMessageHeader(messageHeaderId);
            var organisation = new OrganisationService().GetAnOrganisation(organisationId);
            var organisation2 = new OrganisationService().GetAnOrganisation(organisation2Id);

            Identifier identifier = new Identifier()
            {
                System = "https://tools.ietf.org/html/rfc4122",
                Value = bundleIdentifierValue,
            };

            var serviceRequestEntry = new Bundle.EntryComponent()
            {
                Resource = serviceRequest,
                FullUrl = serviceRequest.ResourceBase.ToString() + serviceRequest.Id,
            };

            var patientEntry = new Bundle.EntryComponent()
            {
                Resource = patient,
                FullUrl = patient.ResourceBase.ToString() + patient.Id,
            };

            var practitionerEntry = new Bundle.EntryComponent()
            {
                Resource = practitioner,
                FullUrl = practitioner.ResourceBase.ToString() + practitioner.Id,
            };

            var messageHeaderEntry = new Bundle.EntryComponent()
            {
                Resource = messageHeader,
                FullUrl = messageHeader.ResourceBase.ToString() + messageHeader.Id,
            };

            var organisationEntry = new Bundle.EntryComponent()
            {
                Resource = organisation,
                FullUrl = organisation.ResourceBase.ToString() + organisation.Id,
            };

            var organisation2Entry = new Bundle.EntryComponent()
            {
                Resource = organisation2,
                FullUrl = organisation2.ResourceBase.ToString() + organisation2.Id,
            };

            bundleToCreate.Id = Guid.NewGuid().ToString();
            bundleToCreate.Type = Bundle.BundleType.Message;
            bundleToCreate.Meta = new Meta() { ProfileElement = new List<FhirUri> { new FhirUri("https://fhir.hl7.org.uk/StructureDefinition/UKCore-Bundle") } };
            bundleToCreate.Identifier = identifier;
            bundleToCreate.Entry.Add(messageHeaderEntry);
            bundleToCreate.Entry.Add(serviceRequestEntry);
            bundleToCreate.Entry.Add(patientEntry);
            bundleToCreate.Entry.Add(practitionerEntry);
            bundleToCreate.Entry.Add(organisationEntry);
            bundleToCreate.Entry.Add(organisation2Entry);

            Bundle bundle = fhirClient.Create<Bundle>(bundleToCreate);
            //_rabbitMQProducer.sendTestMessage(destination, bundle);

            return bundle;
        }
    }
}
