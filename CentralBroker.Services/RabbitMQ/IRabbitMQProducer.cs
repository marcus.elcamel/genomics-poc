﻿namespace Fhir.RabbitMQ
{
    public interface IRabbitMQProducer
    {
        public void sendTestMessage<T> (string key, T message);
    }
}
