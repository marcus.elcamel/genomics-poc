﻿using Newtonsoft.Json;
using RabbitMQ.Client;
using System.Text;

namespace Fhir.RabbitMQ
{
    public class RabbitMQProducer : IRabbitMQProducer
    {
        public void sendTestMessage<T>(string key, T message)
        {
            var factory = new ConnectionFactory { HostName = "localhost" };

            var connection = factory.CreateConnection();

            using
            var channel = connection.CreateModel();

            channel.ExchangeDeclare(exchange: "myRoutingExchange", ExchangeType.Direct);

            var json = JsonConvert.SerializeObject(message);
            var body = Encoding.UTF8.GetBytes(json);

            channel.BasicPublish(exchange: "myRoutingExchange", routingKey: key, body: body);

            Console.WriteLine($"This message has been routed to {key}");
        }
    }
}
