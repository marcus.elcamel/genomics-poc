﻿using CentralBroker.Services.Interfaces;
using Hl7.Fhir.Model;
using Hl7.Fhir.Rest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CentralBroker.Services
{
    public class ServiceRequestService : IServiceRequestService
    {
        public ServiceRequestService() { }

        FhirClientSettings settings = new FhirClientSettings
        {
            PreferredFormat = ResourceFormat.Json,
            PreferredReturn = Prefer.ReturnRepresentation
        };
        public List<ServiceRequest> GetServiceRequests()
        {
            FhirClient fhirClient = new FhirClient("https://server.fire.ly/", settings);
            List<ServiceRequest> serviceRequests = new List<ServiceRequest>();

            Bundle serviceRequestBundle = fhirClient.Search<ServiceRequest>();

            foreach (Bundle.EntryComponent entry in serviceRequestBundle.Entry)
            {
                if (entry.Resource is not null)
                {
                    ServiceRequest serviceRequest = (ServiceRequest)entry.Resource;

                    serviceRequests.Add(serviceRequest);
                }
            }
            return serviceRequests;
        }
        public ServiceRequest? GetAServiceRequest(string serviceRequestId)
        {
            FhirClient fhirClient = new FhirClient("https://server.fire.ly/", settings);

            ServiceRequest serviceRequest = fhirClient.Read<ServiceRequest>($"ServiceRequest/{serviceRequestId}");

            return serviceRequest;
        }
        public void DeleteAServiceRequest(string serviceRequestId)
        {
            FhirClient fhirClient = new FhirClient("https://server.fire.ly/", settings);
            ServiceRequest? serviceReqToDelete = GetAServiceRequest(serviceRequestId);

            if (serviceReqToDelete is not null)
            {
                fhirClient.Delete($"ServiceRequest/{serviceRequestId}");
            }
        }
        public ServiceRequest CreateAServiceRequest(RequestStatus requestStatus, RequestIntent requestIntent, string codingCode, string codingDisplay, string subjectReference, string subjectName, string requesterReference, string requesterName, string performerReference, string performerName)
        {
            FhirClient fhirClient = new FhirClient("https://server.fire.ly/", settings);
            ServiceRequest serviceRequestToCreate = new ServiceRequest();

            Identifier serviceRequestIdentifier = new Identifier()
            {
                System = "https://tools.ietf.org/html/rfc4122",
                Value = Guid.NewGuid().ToString(),
            };

            List<Coding> codingList = new List<Coding>();
            Coding codeCoding = new Coding()
            {
                System = "https://www.england.nhs.uk/publication/national-genomic-test-directories/",
                Code = codingCode,
                Display = codingDisplay
            };
            codingList.Add(codeCoding);

            CodeableConcept codeableConcept = new CodeableConcept()
            {
                Coding = codingList,
            };

            ResourceReference subject = new ResourceReference()
            {
                Reference = subjectReference,
                Display = subjectName
            };

            ResourceReference requester = new ResourceReference()
            {
                Reference = requesterReference,
                Display = requesterName
            };

            ResourceReference performer = new ResourceReference()
            {
                Reference = performerReference,
                Display = performerName
            };

            serviceRequestToCreate.Id = Guid.NewGuid().ToString();
            serviceRequestToCreate.Meta = new Meta() { ProfileElement = new List<FhirUri> { new FhirUri("https://fhir.hl7.org.uk/StructureDefinition/UKCore-ServiceRequest") } };
            serviceRequestToCreate.Identifier.Add(serviceRequestIdentifier);
            serviceRequestToCreate.Status = requestStatus;
            serviceRequestToCreate.Intent = requestIntent;
            serviceRequestToCreate.Code = codeableConcept;
            serviceRequestToCreate.Subject = subject;
            serviceRequestToCreate.Requester = requester;
            serviceRequestToCreate.Performer.Add(performer);

            ServiceRequest serviceRequest = fhirClient.Create<ServiceRequest>(serviceRequestToCreate);

            return serviceRequestToCreate;
        }
    }
}
