﻿using CentralBroker.Services.Interfaces;
using Hl7.Fhir.Model;
using Hl7.Fhir.Rest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CentralBroker.Services
{
    public class PractitionerService : IPractitionerService
    {
        public PractitionerService() { }

        FhirClientSettings settings = new FhirClientSettings
        {
            PreferredFormat = ResourceFormat.Json,
            PreferredReturn = Prefer.ReturnRepresentation
        };

        public Practitioner CreateAPractitioner(string practitionerIdentifierValue, HumanName.NameUse nameUse, string familyName, string givenName, string prefix, string suffix)
        {
            FhirClient fhirClient = new FhirClient("https://server.fire.ly/", settings);
            Practitioner practitionerToCreate = new Practitioner();

            Identifier practitionerIdentifier = new Identifier()
            {
                System = "https://fhir.nhs.uk/Id/sds-user-id",
                Value = practitionerIdentifierValue,
            };

            HumanName practitionerName = new HumanName()
            {
                Use = nameUse,
                Family = familyName,
                Given = new List<string>() { givenName },
                Prefix = new List<string>() { prefix },
                Suffix = new List<string>() { suffix }
            };

            practitionerToCreate.Id = Guid.NewGuid().ToString();
            practitionerToCreate.Meta = new Meta() { ProfileElement = new List<FhirUri> { new FhirUri("https://fhir.hl7.org.uk/StructureDefinition/UKCore-Practitioner") } };
            practitionerToCreate.Identifier.Add(practitionerIdentifier);
            practitionerToCreate.Name.Add(practitionerName);

            Practitioner practitioner = fhirClient.Create<Practitioner>(practitionerToCreate);

            return practitioner;
        }

        public void DeleteAPractitioner(string practitionerId)
        {
            FhirClient fhirClient = new FhirClient("https://server.fire.ly/", settings);
            Practitioner practitionerToDelete = GetAPractitioner(practitionerId);

            if (practitionerToDelete is not null)
            {
                fhirClient.Delete($"Practitioner/{practitionerId}");
            }
        }

        public Practitioner GetAPractitioner(string practitionerId)
        {
            FhirClient fhirClient = new FhirClient("https://server.fire.ly/", settings);

            Practitioner practitioner = fhirClient.Read<Practitioner>($"Practitioner/{practitionerId}");

            return practitioner;
        }

        public List<Practitioner> GetPractitioners()
        {
            FhirClient fhirClient = new FhirClient("https://server.fire.ly/", settings);
            List<Practitioner> practitioners = new List<Practitioner>();

            Bundle practitionerBundle = fhirClient.Search<Practitioner>();

            foreach (Bundle.EntryComponent entry in practitionerBundle.Entry)
            {
                if (entry.Resource is not null)
                {
                    Practitioner practitioner = (Practitioner)entry.Resource;

                    practitioners.Add(practitioner);
                }
            }
            return practitioners;
        }
    }
}
