﻿using CentralBroker.Services.Interfaces;
using Hl7.Fhir.Model;
using Hl7.Fhir.Rest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CentralBroker.Services
{
    public class OrganisationService : IOrganisationService
    {
        public OrganisationService() { }

        FhirClientSettings settings = new FhirClientSettings
        {
            PreferredFormat = ResourceFormat.Json,
            PreferredReturn = Prefer.ReturnRepresentation
        };

        public List<Organization> GetOrganisations()
        {
            FhirClient fhirClient = new FhirClient("https://server.fire.ly/", settings);
            List<Organization> organisations = new List<Organization>();

            Bundle organisationBundle = fhirClient.Search<Organization>();

            foreach (Bundle.EntryComponent entry in organisationBundle.Entry)
            {
                if (entry.Resource is not null)
                {
                    Organization organisation = (Organization)entry.Resource;

                    organisations.Add(organisation);
                }
            }
            return organisations;
        }
        public Organization GetAnOrganisation(string organisationId)
        {
            FhirClient fhirClient = new FhirClient("https://server.fire.ly/", settings);

            Organization organization = fhirClient.Read<Organization>($"Organization/{organisationId}");

            return organization;
        }
        public void DeleteAnOrganisation(string organisationId)
        {
            FhirClient fhirClient = new FhirClient("https://server.fire.ly/", settings);
            Organization organisationToDelete = GetAnOrganisation(organisationId);

            if (organisationToDelete is not null)
            {
                fhirClient.Delete($"Organization/{organisationId}");
            }
        }
        public Organization CreateAnOrganisation(string organisationIdentifierValue, string organisationName, string organisationAddressLine, string organisationAddressLine2, string organisationAddressLine3, string organisationCity, string organisationPostalCode)
        {
            FhirClient fhirClient = new FhirClient("https://server.fire.ly/", settings);
            Organization organizationToCreate = new Organization();
            List<string> organisationAddressLines = new List<string>();
            Address organisationAddress = new Address();

            Identifier organisationIdentifier = new Identifier()
            {
                System = "https://fhir.nhs.uk/Id/ods-organization-code",
                Value = organisationIdentifierValue,
            };

            organizationToCreate.Id = Guid.NewGuid().ToString();
            organizationToCreate.Meta = new Meta() { ProfileElement = new List<FhirUri> { new FhirUri("https://fhir.hl7.org.uk/StructureDefinition/UKCore-Organization") } };
            organizationToCreate.Identifier.Add(organisationIdentifier);
            organizationToCreate.Name = organisationName;

            organisationAddressLines.Add(organisationAddressLine);
            organisationAddressLines.Add(organisationAddressLine2);
            organisationAddressLines.Add(organisationAddressLine3);

            organisationAddress.Line = organisationAddressLines;
            organisationAddress.City = organisationCity;
            organisationAddress.PostalCode = organisationPostalCode;
            organizationToCreate.Address.Add(organisationAddress);

            Organization organization = fhirClient.Create<Organization>(organizationToCreate);

            return organization;
        }
        public Organization UpdateAnOrganisation(string organisationId, string newOrganisationId)
        {
            FhirClient fhirClient = new FhirClient("https://server.fire.ly/", settings);
            Organization? organisationToUpdate = GetAnOrganisation(organisationId);

            organisationToUpdate.IdElement = new Id() { Value = newOrganisationId };

            Organization updatedPatient = fhirClient.Update<Organization>(organisationToUpdate);

            return updatedPatient;
        }
    }
}
