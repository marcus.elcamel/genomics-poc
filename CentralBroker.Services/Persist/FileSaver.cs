﻿using Hl7.Fhir.Model;
using Hl7.Fhir.Serialization;
using System.Text.Json;

namespace Fhir.Persist
{
    public class FileSaver
    {
        public FileSaver()
        {
            
        }

        public void save(Resource resourceType)
        {
            var options = new JsonSerializerOptions().ForFhir().Pretty();
            string serviceRequestJson = JsonSerializer.Serialize(resourceType, options).Replace("https://server.fire.ly/", "urn:uuid:");

            using (StreamWriter outputFile = new StreamWriter(Path.Combine("./json", $"{resourceType.Id}.json")))
            {
                outputFile.WriteLine(serviceRequestJson);
            }
        }
    }
}