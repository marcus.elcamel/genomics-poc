﻿using Hl7.Fhir.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CentralBroker.Services.Interfaces
{
    public interface IBundleService
    {
        Bundle GetBundles();
        Bundle GetABundle(string bundleId);
        void DeleteABundle(string bundleId);
        Bundle CreateABundle(
            string bundleIdentifierValue,
            string serviceReqId,
            string patientId,
            string practitionerId,
            string messageHeaderId,
            string organisationId,
            string organisation2Id,
            string destination);
    }
}
