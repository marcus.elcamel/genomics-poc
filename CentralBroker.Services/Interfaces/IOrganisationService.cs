﻿using Hl7.Fhir.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CentralBroker.Services.Interfaces
{
    public interface IOrganisationService
    {
        List<Organization> GetOrganisations();
        Organization GetAnOrganisation(string organizationId);
        void DeleteAnOrganisation(string organizationId);
        Organization CreateAnOrganisation(
            string organisationIdentifierValue,
            string organisationName,
            string organisationAddressLine,
            string organisationAddressLine2,
            string organisationAddressLine3,
            string organisationCity,
            string organisationPostalCode);
        Organization UpdateAnOrganisation(string organisationId, string newOrganisationId);
    }
}
