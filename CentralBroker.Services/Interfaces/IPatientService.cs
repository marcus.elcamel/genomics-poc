﻿using Hl7.Fhir.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CentralBroker.Services.Interfaces
{
    public interface IPatientService
    {
        List<Patient> GetPatients();
        Patient? GetAPatient(string patientId);
        void DeleteAPatient(string patientId);
        Patient CreateAPatient(
            string NHSNumber,
            HumanName.NameUse usualName,
            string familyName,
            string givenName,
            AdministrativeGender gender,
            string dateOfBirth);
        Patient UpdateAPatient(
            string patientId,
            string NHSNumber,
            HumanName.NameUse usualName,
            string familyName,
            string givenName,
            AdministrativeGender gender,
            string dateOfBirth);
    }
}
