﻿using Hl7.Fhir.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CentralBroker.Services.Interfaces
{
    public interface IPractitionerService
    {
        List<Practitioner> GetPractitioners();
        Practitioner GetAPractitioner(string practitionerId);
        void DeleteAPractitioner(string practitionerId);
        Practitioner CreateAPractitioner(string practitionerIdentifierValue, HumanName.NameUse nameUse, string familyName, string givenName, string prefix, string suffix);
    }
}
