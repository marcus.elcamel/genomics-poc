﻿using Hl7.Fhir.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CentralBroker.Services.Interfaces
{
    public interface IMessageHeaderService
    {
        List<MessageHeader> GetMessageHeaders();
        MessageHeader? GetAMessageHeader(string messageHeaderId);
        void DeleteAMessageHeader(string messageHeaderId);
        MessageHeader CreateAMessageHeader(
            string codingSystem,
            string codingCode,
            string codingDisplay,
            string destinationName,
            string destinationEndpoint,
            string resourceDestinationRef,
            string senderReference,
            string sourceName,
            string sourceEndpoint,
            string focusReference);
        MessageHeader UpdateAMessageHeader(
            string bundleId,
            string messageHeaderId,
            string messageHeaderProfile,
            string codingSystem,
            string codingCode,
            string codingDisplay,
            string destinationName,
            string destinationEndpoint,
            string resourceDestinationRef,
            string senderReference,
            string sourceName,
            string sourceEndpoint,
            string focusReference);
    }
}
