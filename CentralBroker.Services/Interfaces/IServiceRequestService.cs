﻿using Hl7.Fhir.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CentralBroker.Services.Interfaces
{
    public interface IServiceRequestService
    {
        List<ServiceRequest> GetServiceRequests();
        ServiceRequest? GetAServiceRequest(string serviceRequestId);
        void DeleteAServiceRequest(string serviceRequestId);
        ServiceRequest CreateAServiceRequest(
            RequestStatus requestStatus,
            RequestIntent requestIntent,
            string codingCode,
            string codingDisplay,
            string subjectReference,
            string subjectName,
            string requesterReference,
            string requesterName,
            string performerReference,
            string performerName);
    }
}
