﻿using CentralBroker.Services.Interfaces;
using Hl7.Fhir.Model;
using Hl7.Fhir.Rest;

namespace CentralBroker.Services
{
    public class PatientService : IPatientService
    {
        public PatientService() { }

        FhirClientSettings settings = new FhirClientSettings
        {
            PreferredFormat = ResourceFormat.Json,
            PreferredReturn = Prefer.ReturnRepresentation
        };

        public List<Patient> GetPatients()
        {
            FhirClient fhirClient = new FhirClient("https://server.fire.ly/", settings);
            List<Patient> patients = new List<Patient>();

            Bundle patientsBundle = fhirClient.Search<Patient>();

            foreach (Bundle.EntryComponent entry in patientsBundle.Entry)
            {
                if (entry.Resource is not null)
                {
                    Patient patient = (Patient)entry.Resource;

                    patients.Add(patient);
                }
            }
            return patients;
        }

        public Patient? GetAPatient(string patientId)
        {
            FhirClient fhirClient = new FhirClient("https://server.fire.ly/", settings);

            Patient patient = fhirClient.Read<Patient>($"Patient/{patientId}");

            return patient;
        }

        public void DeleteAPatient(string patientId)
        {
            FhirClient fhirClient = new FhirClient("https://server.fire.ly/", settings);
            Patient? patientToDelete = GetAPatient(patientId);

            if (patientToDelete is not null)
            {
                fhirClient.Delete($"Patient/{patientId}");
            }
        }

        public Patient CreateAPatient(                                          
            string NHSNumber,
            HumanName.NameUse usualName,
            string familyName,
            string givenName,
            AdministrativeGender gender,
            string dateOfBirth)
        {
            FhirClient fhirClient = new FhirClient("https://server.fire.ly/", settings);

            Identifier identifier = new Identifier()
            {
                SystemElement = new FhirUri("https://fhir.nhs.uk/Id/nhs-number"),
                ValueElement = new FhirString(NHSNumber),
            };

            List<Coding> codingList = new List<Coding>();
            Coding nhsVerificationCode = new Coding()
            {
                SystemElement = new FhirUri("https://fhir.hl7.org.uk/CodeSystem/UKCore-NHSNumberVerificationStatus"),
                CodeElement = new Code("number-present-and-verified"),
                DisplayElement = new FhirString("Number present and verified"),
            };
            codingList.Add(nhsVerificationCode);

            Extension extension = new Extension()
            {
                Url = "https://fhir.hl7.org.uk/StructureDefinition/Extension-UKCore-NHSNumberVerificationStatus",
                Value = new CodeableConcept() { Coding = codingList }
            };

            Patient patientToCreate = new Patient()
            {
                IdElement = new Id() { Value = Guid.NewGuid().ToString() },
                Meta = new Meta()
                {
                    ProfileElement = new List<FhirUri>()
                    {
                        new FhirUri("https://fhir.hl7.org.uk/StructureDefinition/UKCore-Patient")
                    }
                },
                Name = new List<HumanName>()
                {
                    new HumanName()
                    {
                        UseElement = new Code<HumanName.NameUse>(usualName),
                        FamilyElement = new FhirString(familyName),
                        GivenElement = new List<FhirString>() { new FhirString(givenName) }
                    }
                },
                GenderElement = new Code<AdministrativeGender>(gender),
                BirthDateElement = new Date(dateOfBirth),
            };

            identifier.Extension.Add(extension);
            patientToCreate.Identifier.Add(identifier);

            Patient patient = fhirClient.Create<Patient>(patientToCreate);

            return patient;
        }

        public Patient UpdateAPatient(
            string patientId,
            string NHSNumber,
            HumanName.NameUse usualName,
            string familyName,
            string givenName,
            AdministrativeGender gender,
            string dateOfBirth)
        {
            FhirClient fhirClient = new FhirClient("https://server.fire.ly/", settings);
            Patient? patientToUpdate = GetAPatient(patientId);

            Patient patient = new Patient()
            {
                Name = new List<HumanName>()
                {
                    new HumanName()
                    {
                        UseElement = new Code<HumanName.NameUse>(usualName),
                        FamilyElement = new FhirString(familyName),
                        GivenElement = new List<FhirString>() { new FhirString(givenName) }
                    }
                },
                GenderElement = new Code<AdministrativeGender>(gender),
                BirthDateElement = new Date(dateOfBirth),
            };

            Identifier identifier = new Identifier()
            {
                ValueElement = new FhirString(NHSNumber),
            };

            patientToUpdate = patient;
            patientToUpdate.Identifier.Add(identifier);

            Patient updatedPatient = fhirClient.Update<Patient>(patientToUpdate);

            return updatedPatient;
        }
    }
}
