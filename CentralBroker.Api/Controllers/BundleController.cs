﻿using CentralBroker.Services.Interfaces;
using Hl7.Fhir.Model;
using Microsoft.AspNetCore.Mvc;

namespace Fhir.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class BundleController : ControllerBase
    {
        private readonly IBundleService _bundleService;
        public BundleController(IBundleService bundleService)
        {
            _bundleService = bundleService;
        }


        [HttpGet(Name = "ReadBundles")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [Produces("application/fhir+json")]
        public ActionResult<Bundle> GetBundles()
        {
            var bundles = this._bundleService.GetBundles();

            return this.Ok(bundles);
        }

        [HttpGet("{bundleId}", Name = "ReadABundle")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [Produces("application/fhir+json")]
        public ActionResult<Bundle> GetABundle([FromRoute] string bundleId)
        {
            var bundle = this._bundleService.GetABundle(bundleId);

            if (bundle is null)
            {
                return this.NotFound();
            }

            return this.Ok(bundle);
        }

        [HttpDelete("{bundleId}", Name = "Deletebundle")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [Produces("application/fhir+json")]
        public ActionResult DeleteABundle([FromRoute] string bundleId)
        {
            var bundleToDelete = this._bundleService.GetABundle(bundleId);

            if (bundleToDelete is null)
            {
                return NotFound();
            }

            this._bundleService.DeleteABundle(bundleId);

            return this.NoContent();
        }

        [HttpPost(Name = "CreateBundle")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [Produces("application/fhir+json")]
        public ActionResult CreateABundle(
            string bundleIdentifierValue,
            string serviceReqId,
            string patientId,
            string practitionerId,
            string messageHeaderId,
            string organisationId,
            string organisation2Id,
            string destination)
        {
            if (!ModelState.IsValid)
            {
                return this.BadRequest(ModelState.ValidationState);
            }

            var bundleToCreate = this._bundleService.CreateABundle(bundleIdentifierValue, serviceReqId, patientId, practitionerId, messageHeaderId, organisationId, organisation2Id, destination);

            if (bundleToCreate is null)
            {
                var problemDetails = new ProblemDetails();
                problemDetails.Title = $"Bundle \"{bundleToCreate.Id}\" already exists.";
                problemDetails.Status = StatusCodes.Status400BadRequest;
                return this.BadRequest(problemDetails);
            }

            return this.Ok(bundleToCreate);
        }
    }
}
