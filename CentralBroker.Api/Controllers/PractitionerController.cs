﻿using CentralBroker.Services.Interfaces;
using Hl7.Fhir.Model;
using Hl7.Fhir.Rest;
using Microsoft.AspNetCore.Mvc;

namespace Fhir.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class PractitionerController : ControllerBase
    {
        private readonly IPractitionerService _practitionerService;
        public PractitionerController(IPractitionerService practitionerService)
        {
            _practitionerService = practitionerService;
        }

        [HttpGet(Name = "ReadAPractitioner")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [Produces("application/fhir+json")]
        public ActionResult<List<Practitioner>> GetPractitioners()
        {
            var practitioners = this._practitionerService.GetPractitioners();

            return this.Ok(practitioners);
        }

        [HttpGet("{practitionerId}", Name = "ReadPractitioner")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [Produces("application/fhir+json")]
        public ActionResult<Practitioner> GetAPractitioner([FromRoute] string practitionerId)
        {
            var practitioner = this._practitionerService.GetAPractitioner(practitionerId);

            if (practitioner is null)
            {
                this.NotFound();
            }

            return this.Ok(practitioner);
        }

        [HttpDelete("{practitionerId}", Name = "DeletePractitioner")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [Produces("application/fhir+json")]
        public ActionResult DeleteAPractitioner([FromRoute] string practitionerId)
        {
            var practitionerToDelete = this.GetAPractitioner(practitionerId);

            if (practitionerToDelete is null)
            {
                return NotFound();
            }

            this._practitionerService.DeleteAPractitioner(practitionerId);

            return this.NoContent();
        }

        [HttpPost(Name = "CreatePractitioner")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [Produces("application/fhir+json")]
        public ActionResult CreateAPractitioner(
            string practitionerIdentifierValue,
            HumanName.NameUse nameUse,
            string familyName,
            string givenName,
            string prefix,
            string suffix)
        {
            if (!ModelState.IsValid)
            {
                return this.BadRequest(ModelState.ValidationState);
            }

            var practitionerToCreate = this._practitionerService.CreateAPractitioner(practitionerIdentifierValue, nameUse, familyName, givenName, prefix, suffix);
            
            if (practitionerToCreate is null)
            {
                var problemDetails = new ProblemDetails();
                problemDetails.Title = $"Practitioner \"{practitionerToCreate.Id}\" already exists.";
                problemDetails.Status = StatusCodes.Status400BadRequest;
                return this.BadRequest(problemDetails);
            }

            return this.Ok(practitionerToCreate);
        }
    }
}
