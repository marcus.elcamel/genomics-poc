﻿using Fhir.RabbitMQ;
using Hl7.Fhir.Model;
using Hl7.Fhir.Rest;
using Microsoft.AspNetCore.Mvc;
using Fhir.Persist;
using Fhir.Controllers;
using System.Text.Json;
using Hl7.Fhir.Serialization;
using CentralBroker.Services.Interfaces;

namespace FHIR.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ServiceRequestController : ControllerBase
    {
        private readonly IServiceRequestService _serviceRequestService;
        public ServiceRequestController(IServiceRequestService serviceRequestService)
        {
            _serviceRequestService = serviceRequestService;
        }

        [HttpGet(Name = "ReadServiceRequestInstances")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [Produces("application/fhir+json")]
        public ActionResult<List<ServiceRequest>> GetServiceRequests()
        {
            var serviceRequests = this._serviceRequestService.GetServiceRequests();

            return this.Ok(serviceRequests);
        }

        [HttpGet("{serviceRequestId}", Name = "ReadServiceRequest")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [Produces("application/fhir+json")]
        public ActionResult<ServiceRequest> GetAServiceRequest([FromRoute] string serviceRequestId)
        {
            var serviceRequest = this._serviceRequestService.GetAServiceRequest(serviceRequestId);

            if (serviceRequest is null)
            {
                return this.NotFound();
            }

            return this.Ok(serviceRequest);
        }

        [HttpDelete("{serviceRequestId}", Name = "DeleteServiceRequest")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [Produces("application/fhir+json")]
        public ActionResult DeleteAServiceRequest([FromRoute] string serviceRequestId)
        {
            var serviceRequestToDelete = this._serviceRequestService.GetAServiceRequest(serviceRequestId);

            if (serviceRequestToDelete is null)
            {
                return NotFound();
            }

            this._serviceRequestService.DeleteAServiceRequest(serviceRequestId);

            return this.NoContent();
        }

        [HttpPost(Name = "CreateServiceRequest")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [Produces("application/fhir+json")]
        public ActionResult CreateAServiceRequest(
            RequestStatus requestStatus,
            RequestIntent requestIntent,
            string codingCode,
            string codingDisplay,
            string subjectReference,
            string subjectName,
            string requesterReference,
            string requesterName,
            string performerReference,
            string performerName
            )
        {
            if (!ModelState.IsValid)
            {
                return this.BadRequest(ModelState.ValidationState);
            }

            var serviceRequestToCreate = this._serviceRequestService.CreateAServiceRequest(requestStatus, requestIntent, codingCode, codingDisplay, subjectReference, subjectName, requesterReference, requesterName, performerReference, performerName);

            if (serviceRequestToCreate is null)
            {
                var problemDetails = new ProblemDetails();
                problemDetails.Title = $"ServiceRequest \"{serviceRequestToCreate.Id}\" already exists.";
                problemDetails.Status = StatusCodes.Status400BadRequest;
                return this.BadRequest(problemDetails);
            }

            return this.Ok(serviceRequestToCreate);
        }

        /*[HttpPut("status-in-bundle", Name = "UpdateServiceRequestInBundle")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [Produces("application/fhir+json")]
        public ActionResult UpdateAServiceRequestStatusInBundle(string bundleId, string serviceReqId, RequestStatus status)
        {
            FhirClient fhirClient = new FhirClient("https://server.fire.ly/", settings);
            Bundle? queriedBundle = new BundleController(_rabbitMQProducer).GetABundle(bundleId);
            ServiceRequest? queriedServiceRequest = GetAServiceRequest(serviceReqId);
            FileSaver fileSaver = new FileSaver();

            queriedServiceRequest.Status = status;

            ServiceRequest updatedServiceRequest = fhirClient.Update<ServiceRequest>(queriedServiceRequest);

            var index = queriedBundle.Entry.FindIndex(e => e.Resource.TypeName == "ServiceRequest");

            if (index != -1)
            {
                var serviceRequestEntry = new Bundle.EntryComponent()
                {
                    Resource = updatedServiceRequest,
                    FullUrl = updatedServiceRequest.ResourceBase.ToString() + updatedServiceRequest.Id,
                };

                queriedBundle.Entry[index] = serviceRequestEntry;
                Bundle updatedBundle = fhirClient.Update<Bundle>(queriedBundle);
                fileSaver.save(updatedBundle);
            }
            fileSaver.save(updatedServiceRequest);

            return Ok(queriedServiceRequest);
        }*/

        /*[HttpPut("{serviceReqId}/{status}", Name = "UpdateServiceRequest")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [Produces("application/fhir+json")]
        public ActionResult UpdateAServiceRequestStatus(string serviceReqId, string status)
        {
            FhirClient fhirClient = new FhirClient("https://server.fire.ly/", settings);
            ServiceRequest? queriedServiceRequest = GetAServiceRequest(serviceReqId);
            FileSaver fileSaver = new FileSaver();

            switch (status)
            {
                case "Unknown":
                    queriedServiceRequest.Status = RequestStatus.Unknown;
                    break;
                case "Draft":
                    queriedServiceRequest.Status = RequestStatus.Draft;
                    break;
                case "Active":
                    queriedServiceRequest.Status = RequestStatus.Active;
                    break;
                case "OnHold":
                    queriedServiceRequest.Status = RequestStatus.OnHold;
                    break;
                case "Revoked":
                    queriedServiceRequest.Status = RequestStatus.Revoked;
                    break;
                case "Completed":
                    queriedServiceRequest.Status = RequestStatus.Completed;
                    break;
                case "EnteredInError":
                    queriedServiceRequest.Status = RequestStatus.EnteredInError;
                    break;

                default:
                    break;
            }
            ServiceRequest updatedServiceRequest = fhirClient.Update<ServiceRequest>(queriedServiceRequest);
            fileSaver.save(updatedServiceRequest);

            Bundle queriedBundle = new BundleController(_rabbitMQProducer).GetBundles();

            List<Bundle> bundleList = new List<Bundle>();
            foreach (Bundle.EntryComponent resource in queriedBundle.Entry)
            {
                bundleList.Add((Bundle)resource.Resource);
            }

            foreach (Bundle bundle in bundleList)
            {
                var options = new JsonSerializerOptions().Pretty();
                string bundleJson = JsonSerializer.Serialize(bundle, options);
                Console.WriteLine(bundleJson.GetType());
                Console.WriteLine("--------------------------------------------------------");

                var options = new JsonSerializerOptions().Pretty();
                Console.WriteLine("----------------------------------------------------------------------------------------------------------------");
                string Json2 = JsonSerializer.Serialize(entry.Entry[1], options);
                Console.WriteLine(Json2);
            }

            var index = queriedBundle.Entry.FindIndex(e => e.Resource.TypeName == "ServiceRequest");

            if (index != -1)
            {
                var serviceRequestEntry = new Bundle.EntryComponent()
                {
                    Resource = updatedServiceRequest,
                    FullUrl = updatedServiceRequest.ResourceBase.ToString() + updatedServiceRequest.Id,
                };

                queriedBundle.Entry[index] = serviceRequestEntry;
                Bundle updatedBundle = fhirClient.Update<Bundle>(queriedBundle);
                fileSaver.save(updatedBundle);
            }

            return Ok(queriedServiceRequest);
        }*/

        /*[HttpGet("tests/{patientId}", Name = "ReadServiceRequestFromPatient")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [Produces("application/fhir+json")]
        public List<ServiceRequest>? GetServiceRequestsFromPatient(string patientId)
        {
            FhirClient fhirClient = new FhirClient("https://server.fire.ly/", settings);
            List<ServiceRequest> serviceRequestList = new List<ServiceRequest>();

            Patient patient = fhirClient.Read<Patient>($"Patient/{patientId}");

            Bundle serviceRequestBundle = fhirClient.Search<ServiceRequest>();

            foreach (Bundle.EntryComponent entry in serviceRequestBundle.Entry)
            {
                if (entry.Resource is not null)
                {
                    ServiceRequest serviceRequest = (ServiceRequest)entry.Resource;

                    if (serviceRequest.Subject.Reference.Replace("https://server.fire.ly/", "") == patient.Id)
                    {
                        serviceRequestList.Add(serviceRequest);
                    }
                }
            }
            return serviceRequestList;
        }*/

        /*[HttpGet("{serviceReqId}/bundle", Name = "ReadServiceRequestFromBundle")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [Produces("application/fhir+json")]
        public Bundle GetBundleFromServiceRequest(string serviceReqId)
        {
            Bundle queriedBundle = new BundleController(_rabbitMQProducer).GetBundles();
            Bundle matchingBundle = new Bundle();
            List<Bundle> bundleList = new List<Bundle>();
            List<Bundle> messageBundleList = new List<Bundle>();

            foreach (Bundle.EntryComponent resource in queriedBundle.Entry)
            {
                bundleList.Add((Bundle)resource.Resource);
            }

            Bundle.BundleType type = Bundle.BundleType.Message;

            for (int entryIndex = 0; entryIndex < bundleList.Count; entryIndex++)
            {
                if (bundleList[entryIndex].TypeElement.Value == type)
                {
                    messageBundleList.Add(bundleList[entryIndex]);
                }
            }

            foreach (Bundle bundle in messageBundleList)
            {
                if (bundle.Meta.ProfileElement.Any())
                {
                    int index = bundle.Meta.ProfileElement.FindIndex(e => e.Any());

                    if (bundle.Meta.ProfileElement[index].Value == "https://fhir.hl7.org.uk/StructureDefinition/UKCore-Bundle" && bundle.Entry.Any(e => e.Resource.Id == serviceReqId))
                    {
                        int resourceIndex = bundle.Entry.FindIndex(e => e.Resource.Id == serviceReqId);
                        if (resourceIndex != -1)
                        {
                            Console.WriteLine(bundle.Entry[resourceIndex].Resource.Id);
                            Console.WriteLine(bundle.Id);
                        }

                        matchingBundle = bundle;
                    }
                }
            }

            var options = new JsonSerializerOptions().Pretty();
            string bundleJson = JsonSerializer.Serialize(matchingBundle, options);

            return matchingBundle;
        }*/

        /*[HttpPut("{serviceReqId}/specimen/{specimen}", Name = "UpdateServiceRequestSpecimen")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [Produces("application/fhir+json")]
        public ActionResult UpdateAServiceRequestSpecimen(string serviceReqId, string specimen)
        {
            FhirClient fhirClient = new FhirClient("https://server.fire.ly/", settings);
            ServiceRequest queriedServiceRequest = GetAServiceRequest(serviceReqId);
            FileSaver fileSaver = new FileSaver();

            List<ResourceReference> resourceList = new List<ResourceReference>();

            resourceList.Add(new ResourceReference() { Reference = "https://fhir.hl7.org.uk/StructureDefinition/UKCore-Specimen", Display = specimen });

            queriedServiceRequest.Specimen = resourceList;

            ServiceRequest updatedServiceRequest = fhirClient.Update<ServiceRequest>(queriedServiceRequest);
            fileSaver.save(updatedServiceRequest);

            Bundle queriedBundle = new BundleController(_rabbitMQProducer).GetBundles();

            List<Bundle> bundleList = new List<Bundle>();
            foreach (Bundle.EntryComponent resource in queriedBundle.Entry)
            {
                bundleList.Add((Bundle)resource.Resource);
            }

            foreach (Bundle bundle in bundleList)
            {
                var options = new JsonSerializerOptions().Pretty();
                string bundleJson = JsonSerializer.Serialize(bundle, options);
                Console.WriteLine(bundleJson.GetType());
                Console.WriteLine("--------------------------------------------------------");

                var options = new JsonSerializerOptions().Pretty();
                Console.WriteLine("----------------------------------------------------------------------------------------------------------------");
                string Json2 = JsonSerializer.Serialize(entry.Entry[1], options);
                Console.WriteLine(Json2);
            }

            var index = queriedBundle.Entry.FindIndex(e => e.Resource.TypeName == "ServiceRequest");

            if (index != -1)
            {
                var serviceRequestEntry = new Bundle.EntryComponent()
                {
                    Resource = updatedServiceRequest,
                    FullUrl = updatedServiceRequest.ResourceBase.ToString() + updatedServiceRequest.Id,
                };

                queriedBundle.Entry[index] = serviceRequestEntry;
                Bundle updatedBundle = fhirClient.Update<Bundle>(queriedBundle);
                fileSaver.save(updatedBundle);
            }

            return Ok(updatedServiceRequest);
        }*/
    }
}

