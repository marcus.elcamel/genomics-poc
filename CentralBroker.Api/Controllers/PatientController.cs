﻿using CentralBroker.Services.Interfaces;
using Fhir.Persist;
using Fhir.RabbitMQ;
using Hl7.Fhir.Model;
using Hl7.Fhir.Rest;
using Microsoft.AspNetCore.Mvc;

namespace Fhir.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class PatientController : ControllerBase
    {
        private readonly IPatientService _patientService;
        public PatientController(IPatientService patientService)
        {
            _patientService = patientService;
        }

        [HttpGet(Name = "ReadPatientInstances")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [Produces("application/fhir+json")]
        public ActionResult<List<Patient>> GetPatients()
        {
            var patients = this._patientService.GetPatients();

            return this.Ok(patients);
        }

        [HttpGet("{patientId}", Name = "ReadPatient")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [Produces("application/fhir+json")]
        public ActionResult<Patient> GetAPatient([FromRoute] string patientId)
        {
            var patient = this._patientService.GetAPatient(patientId);

            if (patient is null)
            {
                return this.NotFound();
            }

            return this.Ok(patient);
        }

        [HttpDelete("{patientId}", Name = "DeletePatientInstance")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [Produces("application/fhir+json")]
        public ActionResult DeleteAPatient([FromRoute] string patientId)
        {
            var patientToDelete = this._patientService.GetAPatient(patientId);

            if (patientToDelete is null)
            {
                return NotFound();
            }

            this._patientService.DeleteAPatient(patientId);

            return this.NoContent();
        }

        [HttpPost(Name = "CreatePatient")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [Produces("application/fhir+json")]
        public ActionResult CreateAPatient(
            string NHSNumber,
            HumanName.NameUse usualName,
            string familyName,
            string givenName,
            AdministrativeGender gender,
            string dateOfBirth)
        {
            if (!ModelState.IsValid)
            {
                return this.BadRequest(ModelState.ValidationState);
            }

            var patientToCreate = this._patientService.CreateAPatient(NHSNumber, usualName, familyName, givenName, gender, dateOfBirth);

            if (patientToCreate is null)
            {
                var problemDetails = new ProblemDetails();
                problemDetails.Title = $"Patient \"{patientToCreate.Id}\" already exists.";
                problemDetails.Status = StatusCodes.Status400BadRequest;
                return this.BadRequest(problemDetails);
            }

            return this.Ok(patientToCreate);
        }

        [HttpPut("{patientId}/{newPatientId}", Name = "UpdateApatientId")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [Produces("application/fhir+json")]
        public ActionResult UpdateAPatient(
            string patientId,
            string NHSNumber,
            HumanName.NameUse usualName,
            string familyName,
            string givenName,
            AdministrativeGender gender,
            string dateOfBirth)
        {
            if (!ModelState.IsValid)
            {
                return this.BadRequest(ModelState.ValidationState);
            }

            var patientToUpdate = this._patientService.UpdateAPatient(patientId, NHSNumber, usualName, familyName, givenName, gender, dateOfBirth);

            return this.Ok(patientToUpdate);
        }
    }
}
