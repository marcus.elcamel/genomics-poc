﻿using CentralBroker.Services.Interfaces;
using Hl7.Fhir.Model;
using Hl7.Fhir.Rest;
using Microsoft.AspNetCore.Mvc;

namespace Fhir.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class MessageHeaderController : ControllerBase
    {
        private readonly IMessageHeaderService _messageHeaderService;
        public MessageHeaderController(IMessageHeaderService messageHeaderService)
        {
            _messageHeaderService = messageHeaderService;
        }

        [HttpGet(Name = "ReadMessageHeaders")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [Produces("application/fhir+json")]
        public ActionResult<List<Practitioner>> GetMessageHeaders()
        {
            var messageHeaders = this._messageHeaderService.GetMessageHeaders();

            return this.Ok(messageHeaders);
        }

        [HttpGet("{messageHeaderId}", Name = "ReadMessageHeader")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [Produces("application/fhir+json")]
        public ActionResult<MessageHeader> GetAMessageHeader([FromRoute] string messageHeaderId)
        {
            var messageHeader = this._messageHeaderService.GetAMessageHeader(messageHeaderId);

            if (messageHeader is null)
            {
                return this.NotFound();
            }

            return this.Ok(messageHeader);
        }

        [HttpDelete("{messageHeaderId}", Name = "DeleteMessageHeader")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [Produces("application/fhir+json")]
        public ActionResult DeleteAMessageHeader([FromRoute] string messageHeaderId)
        {
            var messageHeaderToDelete = this._messageHeaderService.GetAMessageHeader(messageHeaderId);

            if (messageHeaderToDelete is null)
            {
                return NotFound();
            }

            this._messageHeaderService.DeleteAMessageHeader(messageHeaderId);

            return this.NoContent();
        }

        [HttpPost(Name = "CreateMessageHeader")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [Produces("application/fhir+json")]
        public ActionResult CreateAMessageHeader(
            string codingSystem,
            string codingCode,
            string codingDisplay,
            string destinationName,
            string destinationEndpoint,
            string resourceDestinationRef,
            string senderReference,
            string sourceName,
            string sourceEndpoint,
            string focusReference)
        {
            if (!ModelState.IsValid)
            {
                return this.BadRequest(ModelState.ValidationState);
            }

            var messageHeaderToCreate = this._messageHeaderService.CreateAMessageHeader(codingSystem, codingCode, codingDisplay, destinationName, destinationEndpoint, resourceDestinationRef, senderReference, sourceName, sourceEndpoint, focusReference);

            if (messageHeaderToCreate is null)
            {
                var problemDetails = new ProblemDetails();
                problemDetails.Title = $"Message header \"{messageHeaderToCreate.Id}\" already exists.";
                problemDetails.Status = StatusCodes.Status400BadRequest;
                return this.BadRequest(problemDetails);
            }

            return this.Ok(messageHeaderToCreate);
        }

        [HttpPut(Name = "UpdateMessageHeader")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [Produces("application/fhir+json")]
        public ActionResult UpdateAMessageHeader(
            string bundleId,
            string messageHeaderId,
            string messageHeaderProfile,
            string codingSystem,
            string codingCode,
            string codingDisplay,
            string destinationName,
            string destinationEndpoint,
            string resourceDestinationRef,
            string senderReference,
            string sourceName,
            string sourceEndpoint,
            string focusReference)
        {
            if (!ModelState.IsValid)
            {
                return this.BadRequest(ModelState.ValidationState);
            }

            var patientToUpdate = this._messageHeaderService.UpdateAMessageHeader(
                bundleId,
                messageHeaderId,
                messageHeaderProfile,
                codingSystem,
                codingCode,
                codingDisplay,
                destinationName,
                destinationEndpoint,
                resourceDestinationRef,
                senderReference,
                sourceName,
                sourceEndpoint, focusReference);

            return this.Ok(patientToUpdate);
        }
    }
}
