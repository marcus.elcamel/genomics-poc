﻿using CentralBroker.Services.Interfaces;
using Fhir.Persist;
using Fhir.RabbitMQ;
using Hl7.Fhir.Model;
using Hl7.Fhir.Rest;
using Microsoft.AspNetCore.Mvc;

namespace Fhir.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class OrganisationController : ControllerBase
    {
        private readonly IOrganisationService _organisationService;

        public OrganisationController(IOrganisationService organisationService)
        {
            _organisationService = organisationService;
        }

        [HttpGet(Name = "ReadAnOrganisation")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [Produces("application/fhir+json")]
        public ActionResult<List<Organization>> GetOrganisations()
        {
            var organisations = this._organisationService.GetOrganisations();

            return this.Ok(organisations);
        }

        [HttpGet("{organisationId}", Name = "ReadOrganisation")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [Produces("application/fhir+json")]
        public ActionResult<Organization> GetAnOrganisation([FromRoute] string organisationId)
        {
            var organisation = this._organisationService.GetAnOrganisation(organisationId);

            if (organisation is null)
            {
                return this.NotFound();
            }

            return this.Ok(organisation);
        }

        [HttpDelete("{organisationId}", Name = "DeleteOrganisation")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [Produces("application/fhir+json")]
        public ActionResult DeleteAnOrganisation([FromRoute] string organisationId)
        {
            var organisationToDelete = this._organisationService.GetAnOrganisation(organisationId);

            if (organisationToDelete is null)
            {
                return NotFound();
            }

            this._organisationService.DeleteAnOrganisation(organisationId);

            return this.NoContent();
        }

        [HttpPost(Name = "CreateOrganisation")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [Produces("application/fhir+json")]
        public ActionResult CreateAnOrganisation(
            string organisationIdentifierValue,
            string organisationName,
            string organisationAddressLine,
            string organisationAddressLine2,
            string organisationAddressLine3,
            string organisationCity,
            string organisationPostalCode)
        {
            if (!ModelState.IsValid)
            {
                return this.BadRequest(ModelState.ValidationState);
            }

            var organisationToCreate = this._organisationService.CreateAnOrganisation(organisationIdentifierValue, organisationName, organisationAddressLine, organisationAddressLine2, organisationAddressLine3, organisationCity, organisationPostalCode);


            if (organisationToCreate is null)
            {
                var problemDetails = new ProblemDetails();
                problemDetails.Title = $"Organisation \"{organisationToCreate.Id}\" already exists.";
                problemDetails.Status = StatusCodes.Status400BadRequest;
                return this.BadRequest(problemDetails);
            }

            return this.Ok(organisationToCreate);
        }

        [HttpPut("{organisationId}/{newOrganisationId}", Name = "UpdateOrganisation")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [Produces("application/fhir+json")]
        public ActionResult UpdateAnOrganisationId(
            string organisationId,
            string newOrganisationId)
        {
            if (!ModelState.IsValid)
            {
                return this.BadRequest(ModelState.ValidationState);
            }

            var organisationToUpdate = this._organisationService.UpdateAnOrganisation(organisationId, newOrganisationId);

            return this.Ok(organisationToUpdate);
        }
    }
}
